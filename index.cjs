const csvtojson = require("csvtojson");//import csv to json package
const teams = require("./src/server/matches won per team.cjs")
const fs = require("fs");//package
const path = require("path")//package

//Paths
const matchesPerCity = require("./src/server/matchesPerCity.cjs");
const matchesPerTeam = require("./src/server/matches won per team.cjs");
const numberOfRedCards = require("./src/server/newRedcard")
const topTenPlayers=require("./src/server/topPlayers.cjs");
const matchesperpoint = require('./src/server/matchesperpoint.cjs')



// Matches_per_City

csvtojson().fromFile("./src/data/WorldCupMatches.csv").then((jsonObj) => {//converts csv to json
    let matches = matchesPerCity(jsonObj);//calling the function and jsonObj is a variable
    fs.writeFileSync(path.join(__dirname,"./src/public/output/1-matches_per_city.json"), JSON.stringify(matches))

})

// Matches_won_per_Team

csvtojson().fromFile("./src/data/WorldCupMatches.csv").then((Matches_Won) => {
    //converts csv to json
    const match = Matches_Won.filter(element => element.City !== "");
    let Teams = matchesPerTeam(match);//calling the function and jsonObj is a variable
    fs.writeFileSync (path.join(__dirname,"./src/public/output/2-matches_won_team.json"), JSON.stringify(Teams))

})

// RedCard

csvtojson().fromFile("./src/data/WorldCupMatches.csv").then((WorldCupMatches) => {
    csvtojson().fromFile("./src/data/WorldCupPlayers.csv").then ((WorldCupPlayers) =>{
    
//         //converts csv to json
       
    
    let Redcard_perTeam = numberOfRedCards(WorldCupPlayers,WorldCupMatches);
    
        //calling the function and jsonObj is a variable
        // console.log(matches);
        
    fs.writeFileSync(path.join(__dirname,"./src/public/output/3-Redcard_perTeam.json"), JSON.stringify(Redcard_perTeam))

})
})
 
//top 10 Players

csvtojson().fromFile("./src/data/WorldCupPlayers.csv").then((WorldCupPlayers) => {//converts csv to json
    let top10Players =topTenPlayers(WorldCupPlayers);//calling the function and jsonObj is a variable
    fs.writeFileSync(path.join(__dirname,"./src/public/output/4-Top-10-Players.json"), JSON.stringify(top10Players))

});

 

csvtojson().fromFile("./src/data/WorldCups.csv").then((WorldCups) => {//converts csv to json
        let WorldCupsPerPoint = matchesperpoint(WorldCups);//calling the function and jsonObj is a variable
        fs.writeFileSync(path.join(__dirname,"./src/public/output/4-WorldCupsPerPoint.json"), JSON.stringify(WorldCupsPerPoint))
    
    });