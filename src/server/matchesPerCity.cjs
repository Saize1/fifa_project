// 1. Number of matches played per city

// function matchesPerCity(jsonObj) {
//   let obj = {};
//   for (let index = 0; index < jsonObj.length; index++) {
//     if (jsonObj[index].City in obj) {
//       obj[jsonObj[index].City] +=1;
//     } else {
//       obj[jsonObj[index].City] = 1;
//     }
//   }

//   return obj;
// }

// module.exports = matchesPerCity;

// function matchesPerCity(WorldCupMatches){
//   function updateData(obj,City){
//     if(City in obj ){
//       obj[City]++;
//     }
//     else{
//       obj[City]=1;
//     }
   
//   }
//   return WorldCupMatches.reduce((acc,element)=>{
//     updateData(acc, element.City);
//     return acc;
// },{})
// }
function matchesPerCity(matchesCity){
  let obj={};
  for(let index=0;index<matchesCity.length;index++ ){
    if(matchesCity[index].City in obj){
    obj[matchesCity[index].City]++;
    }
    else{
      obj[matchesCity[index].City] =1;
    }
  }
  // console.log(obj);
  return obj;
}
module.exports = matchesPerCity
