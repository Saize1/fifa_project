function topTenPlayers(WorldCupPlayers) {
  let matchesplayed = {};
  let goals = {};
  let probability = {};
  let final = {};
  for (let i = 0; i < WorldCupPlayers.length; i++) {
    let player = WorldCupPlayers[i]["Player Name"];
    if (player in matchesplayed) {
      matchesplayed[player] += 1;
    } else {
      matchesplayed[player] = 1;
    }
  }

  // console.log(WorldCupPlayers[2].Event);
  for (let i = 0; i < WorldCupPlayers.length; i++) {
    let player = WorldCupPlayers[i]["Player Name"];
    if (!WorldCupPlayers[i].Event) {
      continue;
    }
    if (WorldCupPlayers[i].Event.includes("G")) {
      if (player in goals) {
        goals[player] += 1;
      } else {
        goals[player] = 1;
      }
    }
  }

  // console.log(goals);
  // let arr = [];
  for (let player in matchesplayed) {
    if (goals[player] === undefined) {
      probability[player] = 0;
    } else {
      probability[player] = goals[player] / matchesplayed[player];
    }
    // for (let player in probability) {
    //   arr.push([player, probability[player]]);
    // }

    // let length = (arr.length - 1);

    // for (let index = length; index >= (length - 10); index--) {

    //     final[arr[index][1]] = arr[index][0];
    // }
  }
  let sort=Object.entries(probability).sort((a,b)=>{
    return a[1]-b[1];
  })
  // console.log(sort);
 

  return sort;
}

// let arr=[];
// for(WorldCupPlayers[i]["Player Name"] in probability){
//     arr.push(WorldCupPlayers[i]["Player Name"])
// }

// arr.sort();
// let length=arr.length-1;
// for(let i=length;i>=(length-10);i++){
//        final[arr[i][1]]=arr[i][0];
// }

//   let sorted = Object.entries(probability).sort(([,a],[,b])=>(a-b));
//   let sortarr=sorted.length-1;
//   let arr=[];
// for(let i=sortarr;i>sortarr-10;i--){

//   // console.log (goals);
// arr.push(sortarr)
// }
// console.log(arr)

module.exports = topTenPlayers;
