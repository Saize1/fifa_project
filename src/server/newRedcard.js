// function numberOfRedCards(WorldCupPlayers, WorldCupMatches) {
//   let year = 2014;
//   let obj = {};

//   function getName(matchID, teamInitial) {
//     let team = WorldCupMatches.find((element) => {
//       return element.MatchID === matchID;
//     });

//     if (year != team.Year) return null;

//     if (teamInitial === team["Home Team Initials"]) {
//       return team["Home Team Name"];
//     } else {
//       return team["Away Team Name"];
//     }
//   }

//   for (let index = 0; index < WorldCupPlayers.length; index++) {
//     if (WorldCupPlayers[index].Event.includes("R")) {
//       let teamName = getName(
//         WorldCupPlayers[index].MatchID,
//         WorldCupPlayers[index]["Team Initials"]
//       );
//       if (!teamName) continue;
//       if (teamName in obj) {
//         obj[teamName]++;
//       } else {
//         obj[teamName] = 1;
//       }
//     }
//   }

//   return obj;
// }
function numberOfRedCards(WorldCupPlayers, WorldCupMatches) {
  let year = 2014;
  let obj = {};
  function getName(matchID, teamInitials) {
    let team = WorldCupMatches.find((element) => {
      return element.MatchID === matchID;
    });
    if (year != team.Year) {
      return null;
    }
    if (teamInitials === team["Home Team Initials"]) {
      return team["Home Team Name"];
    } else {
      return team["Away Team Name"];
    }
  }
  for (let index = 0; index < WorldCupPlayers.length; index++) {
    if (WorldCupPlayers[index].Event.includes("R")) {
      let Team = getName(
        WorldCupPlayers[index].MatchID,
        WorldCupPlayers[index]["Team Initials"]
      );

      if (!Team) {
        continue;
      }
      if (Team in obj) {
        obj[Team]++;
      } else {
        obj[Team] = 1;
      }
    }
  }
  return obj;
}

module.exports = numberOfRedCards;
